// class TcpSocket implementation
// KAA 20080909

#include "TcpSocket.h"
#include <iostream>

// if it fails, it sets ksfd<0 , which should block all other methods

// constructor
TcpSocket::TcpSocket(const char *hname, unsigned short usport)
{
  ksfd = -1;   // indicate the socket not created
  timout = 5.0; // set the default time-out in seconds

  // get the IP-address
  ptrh = gethostbyname(hname);
  if( ptrh == NULL ) {
    printf("Invalid host: %s\n", hname);
    return;
  }

  memset((char *)&sad,0,sizeof(sad)); /* clear sockaddr structure */
  memcpy(&sad.sin_addr, ptrh->h_addr, ptrh->h_length);
  sad.sin_family = AF_INET;

  // port 5000...
  sad.sin_port=htons(usport);

  // create the socket
  ksfd = socket(AF_INET, SOCK_STREAM, 0);
  if(ksfd<0){
    printf("error creating a socket\n");
    return;
  }

// set the socket time-out
  SetTimeOut(timout);

  // connect the socket
  if( connect(ksfd, (struct sockaddr *)&sad, sizeof(sockaddr)) < 0) {
    printf("socket connect to %s failed\n",hname);
    // close the socket, indicate failure and return
    close(ksfd);
    ksfd = -1;
    return;
  }
}

TcpSocket::~TcpSocket() // destructor
{
  int irltd;
  if(ksfd>=0){
    irltd = close(ksfd);
    if(irltd!=0){
      printf("error closing the socket\n");
    }
  }
    
}

int TcpSocket::SetTimeOut(double secs)
{
  struct timeval stimout={0,0};  // socket timeout 5 sec
  int irlt;

  double tims,timu;

  // if the socket was not created exit with error
  if(ksfd<0) return ksfd;  

  timout = secs;  // store the time-out value in a private variable

  timu = modf(secs,&tims); // decompose to integer and fractional parts

  stimout.tv_sec = (int)tims;                // set seconds
  stimout.tv_usec = (int)(1000000.0*timu);   // and microseconds
  
  irlt = setsockopt(ksfd, SOL_SOCKET, SO_RCVTIMEO, &stimout, sizeof(stimout));
  if(irlt<0){
    printf("failed to set the socket time-out\n");
  }
  return irlt;
}

int TcpSocket::WriteRead(char *outbf, int outbflng, char *inbf, int inbflng)
{
  int irlt;

  // if the socket was not created exit with error
  if(ksfd<0) return -1;  

  //write
  irlt = send(ksfd, outbf, outbflng, 0);
  if(irlt<0){
    printf("socket write error: %s\n",strerror(errno));
	inbf=strerror(errno);
    return -1;
  }

  //read
  irlt = recv(ksfd, inbf, inbflng, 0);
  if(irlt>0) return irlt;
  if(irlt<0){
    printf("socket read error: %s\n",strerror(errno));
	strcpy(inbf,strerror(errno));
    return -1;
  }
  
  if(errno==11) return 0;  // read time-out  - probably, it is LINUX specific
  if(irlt==0) return -99;  // host disconnected 
  return irlt;
  
}

int TcpSocket::Wrt(char *outbf, int outbflng)
{
  int irlt;

  // if the socket was not created exit with error
  if(ksfd<0) return -1;  

  //write
  irlt = send(ksfd, outbf, outbflng, 0);
  if(irlt<0){
    printf("socket write error: %s\n",strerror(errno));
  } 
  return irlt;
}

int TcpSocket::FlushInput()
{
  int irlt,irlt1,iretcd;
  fd_set rfds;
  struct timeval tv;
  char intbf[1000];

  // if the socket was not created exit with error
  if(ksfd<0) return -1;  

  iretcd=0;
  do {
    FD_ZERO(&rfds);
    FD_SET(ksfd, &rfds);

    // set thetimeout 0.3 sec - hopefully it is enough for the remote
    // to prepare the next block of data if it really wants to
    tv.tv_sec = 0;
    tv.tv_usec = 300000;

    // test if there is data to read
    irlt = select(1+ksfd, &rfds, NULL, NULL, &tv);

    if(irlt<0) return -1; // error return

    if(irlt>0){

      // try to read
      irlt1 = recv(ksfd, intbf, 1000, 0);

      if(irlt1<=0) return -1; // error while flushing
      else iretcd=1;         // indicate that we have read something

    }

  } while (irlt>0);

  return iretcd; // 0:no flush/1:flush happened
  
}
