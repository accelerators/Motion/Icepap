// class TcpSocket
// KAA 20080909

#ifndef _TCPSOCKET
#define _TCPSOCKET

#include <netdb.h>   //gethostbyname
#include <stdlib.h>  //exit
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <math.h>   // modf
#include <poll.h>
#include <sys/socket.h>

class TcpSocket
{
 private:
  int ksfd;        // socket file descriptor
  double timout;   // socket time-out in seconds
  struct  hostent  *ptrh;  /* pointer to a host table entry       */
  struct  sockaddr_in sad; /* structure to hold an IP address     */
         
 public:

  TcpSocket(const char *hname, unsigned short usport); //constructor
  ~TcpSocket();

  int get_fd(){ return ksfd;};

  // outputs outbflng bytes from outbf, and reads inbf max inbflng bytes
  // returns #of bytes read/0:read time out/-1:write error/-2:read error
  //
  int WriteRead(char *outbf, int outbflng, char *inbf, int inbflng);

  // outputs outbflng bytes from outbf
  // returns #of bytes sent, or -1 if error occures
  // 
  int Wrt(char *outbf, int outbflng);

  // sets the socket time-out in seconds
  // returns 0-OK/-1 - error
  int SetTimeOut(double dtimout);

  // flush the input stream
  // returns 1:stream was flushed/0:stream was empty/-1:error
  int FlushInput();

};

#endif
