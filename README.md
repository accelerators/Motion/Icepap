# IcePap Tango Device Server

Project around Icepap motor controller
In this project you have different Tango Device Server

the main server is IcePapController contains Classes : IcePapController, IcePapMotor, MultiplesAxes

the second main server is GirderJacks contains Classes : IcePapController, GirderJacks, MultiplesAxes
	IcePapMotors is replace by GirderJacks specialization of IcePapMotor for Girder of Ebs ESRF
And also ExentricMotors

Other Class can be can compile alone
	MultipleAxes

## Cloning

Any instructions or special flags required to clone the project. For example, if the project has submodules in git, then specify this and give the command to check them out correctly, example:

```
git clone --recurse-submodules git@gitlab.esrf.fr:accelerators/Motion/Icepap.git
```

## Documentation

Pogo generated documentation in **doc_html** folder

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies

* Tango Controls 9 or higher.
* omniORB release 4 or higher.
* libzmq - libzmq3-dev or libzmq5-dev.

#### Toolchain Dependencies

* C++11 compliant compiler.

### Build

Instructions on building the project.

Make example:

For IcePapController
```bash
make
```

For GirderJacks:
```bash
cd GirderJacks
make
```

For ExcentricMotor:
```bash
cd excentricmotor
make
```


For MultipleAxes:
```bash
cd MultipleAxes
make
```
