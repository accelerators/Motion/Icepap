
# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.3.1] - 26-07-2022
### modified
* finaly remove stopcode for abort stopcode only

## [1.3.0] - 07-07-2022
### added
* add attribute steps motor read only
* exploits the register stopcode, for add status (and ALARM state) message witch gives the reason for stop motor, if it is the case.
### fixed
* fix forbiden movement around limit switch test little bit open

## [1.2.6] - 15-02-2022
### added
* add in MultipleAxies get_channel_indices do the same as get_channel_index but for a vector

## [1.2.5] - 13-12-2021
### Fixed
* Fix crash at delete device

## [1.2.3] - 10-03-2021
### Fixed
* Fix bug with abort when backlash was set to non-zero value.
The movement was not aborted when the motor was moving in a direction requiring backlash.

## [1.2.2] - 09-10-2019

### Changed
* add WagoAdc and Modbus Tango Class for ExcentricMotor


## [1.2.1] - 13-09-2019

### Changed
* fix serval Warning at compile (-Wall)
* fix wrong type for Home_side
* Suppress some unless attributes
* Clean interface in Pogo (some unless abstract attributes)
* fix IPMask reading for lock attribute
* add isMoving Attribute


## [1.2.0] - 04-07-2019

### Changed
* fix several errors in valgrind
* fix some mini memory leek
* fix the amounth of the logs in case of no conection with the icepap control
* fix state in case of no connection with the icepap control. Now it is UNKNOWN in place of FAULT
* fix warning at compilation in IcePapControler
