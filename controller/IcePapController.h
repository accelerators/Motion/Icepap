/*----- PROTECTED REGION ID(IcePapController.h) ENABLED START -----*/
//=============================================================================
//
// file :        IcePapController.h
//
// description : Include for the IcePapController class.
//
// project :     .
//
// $Author: jmchaize $
//
// $Revision: 1.3 $
// $Date: 2013/06/20 13:25:07 $
//
// SVN only:
// $HeadURL:  $
//
// CVS only:
// $Source: /cvsroot/tango-ds/Motion/icepap/controller/IcePapController.h,v $
// $Log: IcePapController.h,v $
// Revision 1.3  2013/06/20 13:25:07  jmchaize
// refactor the code, create a raw_cmd command
//
// Revision 1.2  2012/01/05 12:25:24  jmchaize
// migrate to pogo 7
//
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef ICEPAPCONTROLLER_H
#define ICEPAPCONTROLLER_H

#include <sys/time.h>
#include <tango.h>
#include <../TcpSocket/TcpSocket.h> // this is my class for the TCP/IP connection





/*----- PROTECTED REGION END -----*/	//	IcePapController.h

/**
 *  IcePapController class description:
 *    A class to perform tcp/ip connections and data transmissions exchages with
 *    the IcePAP controller unit.
 */

namespace IcePapController_ns
{
/*----- PROTECTED REGION ID(IcePapController::Additional Class Declarations) ENABLED START -----*/

		//		Additional Class Declarations

	/*----- PROTECTED REGION END -----*/	//	IcePapController::Additional Class Declarations

class IcePapController : public TANGO_BASE_CLASS
{

/*----- PROTECTED REGION ID(IcePapController::Data Members) ENABLED START -----*/

	//		Add your own data members
public:
    
        struct timeval tvalstart, tvalend, tvalelapsed;
        Tango::WAttribute* wlock;
        string lastIpMask;


	/*----- PROTECTED REGION END -----*/	//	IcePapController::Data Members

//	Device property data members
public:
	//	IPaddress:	A string containing either the internet host name, or IP address
	//  of the IcePAP controller unit
	string	iPaddress;
	//	Comm_timeout:	A time-out (in seconds) for the TCP/IP communication.
	Tango::DevDouble	comm_timeout;
	//	IPMASK:	give the IP mask for write access.
	//  any client should be inside the IP mask to be allowed to write
	string	iPMASK;
	//	Location:	name of the room hosting the icepap controller
	string	location;
	//	LockedTimeOut:	
	Tango::DevShort	lockedTimeOut;

//	Attribute data members
public:
	Tango::DevString	*attr_Location_read;
	Tango::DevBoolean	*attr_Locked_read;
	Tango::DevShort	*attr_UnlockRemainingTime_read;

//	Constructors and destructors
public:
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	IcePapController(Tango::DeviceClass *cl,string &s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	IcePapController(Tango::DeviceClass *cl,const char *s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device name
	 *	@param d	Device description.
	 */
	IcePapController(Tango::DeviceClass *cl,const char *s,const char *d);
	/**
	 * The device object destructor.
	 */
	~IcePapController() {delete_device();};


//	Miscellaneous methods
public:
	/*
	 *	will be called at device destruction or at init command.
	 */
	void delete_device();
	/*
	 *	Initialize the device
	 */
	virtual void init_device();
	/*
	 *	Read the device properties from database
	 */
	void get_device_property();
	/*
	 *	Always executed method before execution command method.
	 */
	virtual void always_executed_hook();


//	Attribute methods
public:
	//--------------------------------------------------------
	/*
	 *	Method      : IcePapController::read_attr_hardware()
	 *	Description : Hardware acquisition for attributes.
	 */
	//--------------------------------------------------------
	virtual void read_attr_hardware(vector<long> &attr_list);
	//--------------------------------------------------------
	/*
	 *	Method      : IcePapController::write_attr_hardware()
	 *	Description : Hardware writing for attributes.
	 */
	//--------------------------------------------------------
	virtual void write_attr_hardware(vector<long> &attr_list);

/**
 *	Attribute Location related methods
 *	Description: location of the icepap controller (useful in case of need)
 *
 *	Data type:	Tango::DevString
 *	Attr type:	Scalar
 */
	virtual void read_Location(Tango::Attribute &attr);
	virtual bool is_Location_allowed(Tango::AttReqType type);
/**
 *	Attribute Locked related methods
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_Locked(Tango::Attribute &attr);
	virtual void write_Locked(Tango::WAttribute &attr);
	virtual bool is_Locked_allowed(Tango::AttReqType type);
/**
 *	Attribute UnlockRemainingTime related methods
 *	Description: 
 *
 *	Data type:	Tango::DevShort
 *	Attr type:	Scalar
 */
	virtual void read_UnlockRemainingTime(Tango::Attribute &attr);
	virtual bool is_UnlockRemainingTime_allowed(Tango::AttReqType type);


	//--------------------------------------------------------
	/**
	 *	Method      : IcePapController::add_dynamic_attributes()
	 *	Description : Add dynamic attributes if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_attributes();




//	Command related methods
public:
	/**
	 *	Command GetIP related method
	 *	Description: The command to extract an IP address of the IcePAP controller - this is the only command that has sence
	 *               for this device server
	 *
	 *	@returns IP address or host name of the IcePAP controller
	 */
	virtual Tango::DevString get_ip();
	virtual bool is_GetIP_allowed(const CORBA::Any &any);
	/**
	 *	Command GetTimeout related method
	 *	Description: Retrives  from the property  the TCP/IP communications time-out value in sec
	 *
	 *	@returns time-out in sec, for TCP/IP communications
	 */
	virtual Tango::DevDouble get_timeout();
	virtual bool is_GetTimeout_allowed(const CORBA::Any &any);
	/**
	 *	Command raw_cmd related method
	 *	Description: send a command on the socket
	 *
	 *	@param argin command to send
	 *	@returns answer
	 */
	virtual Tango::DevString raw_cmd(Tango::DevString argin);
	virtual bool is_raw_cmd_allowed(const CORBA::Any &any);
	/**
	 *	Command Reboot related method
	 *	Description: hard reboot of the icepap crate, 
	 *               warning this command will block the communication for few seconds
	 *
	 */
	virtual void reboot();
	virtual bool is_Reboot_allowed(const CORBA::Any &any);


	//--------------------------------------------------------
	/**
	 *	Method      : IcePapController::add_dynamic_commands()
	 *	Description : Add dynamic commands if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_commands();

/*----- PROTECTED REGION ID(IcePapController::Additional Method prototypes) ENABLED START -----*/

	//	Additional Method prototypes
protected :	

  private :
 TcpSocket *comm_sock;  // here will be a pointer to the TCP/IP communication
                        // object

 char *reply;           //here is a pointer to the command reply buffer; 
 int BufLng;            //length of "reply"
 string firmware_version;
 bool communication_broken;
 
        Tango::DevString raw_cmd(string argin);
        string readIPMask();
        void rebuildSocket(void);

	/*----- PROTECTED REGION END -----*/	//	IcePapController::Additional Method prototypes
};

/*----- PROTECTED REGION ID(IcePapController::Additional Classes Definitions) ENABLED START -----*/

	//	Additional Classes definitions

	/*----- PROTECTED REGION END -----*/	//	IcePapController::Additional Classes Definitions

}	//	End of namespace

#endif   //	IcePapController_H
