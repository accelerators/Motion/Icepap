//=============================================================================
//
// file :        PosiitonAtt.h
//
// description : Include for the dynamic scalar attribute class.
//
// project :	 MultipleAxes TANGO device server.
//
// $Author: jmchaize $
//
// $Revision: 1.1.1.1 $
//
// $Log: PositionAtt.h,v $
// Revision 1.1.1.1  2009/06/04 09:07:22  jmchaize
// Imported using TkCVS
//
// Revision 1.1.1.1  2007/05/30 09:06:35  chaize
// Imported using TkCVS
//

//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//=============================================================================
#ifndef _POSATTR_H
#define _POSATTR_H

#include "MultipleAxes.h"

namespace MultipleAxes_ns
{
  class PositionAtt: public Tango::Attr
    {
    public:
      PositionAtt(const char *name,long data_type,Tango::AttrWriteType w_type)
	:Attr(name,data_type,w_type) {};
      ~PositionAtt() {};
      
      virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<MultipleAxes *>(dev))->read_pos(att);}
      virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<MultipleAxes *>(dev))->write_pos(att);}
    };
  
}

#endif	// _POSATTR_H
