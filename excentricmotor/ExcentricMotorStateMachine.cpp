/*----- PROTECTED REGION ID(ExcentricMotorStateMachine.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        ExcentricMotorStateMachine.cpp
//
// description : State machine file for the ExcentricMotor class
//
// project :     ExcentricMotor
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author: chaize $
//
// $Revision: 1.1.1.1 $
// $Date: 2013/04/10 15:05:57 $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

#include <ExcentricMotor.h>

/*----- PROTECTED REGION END -----*/	//	ExcentricMotor::ExcentricMotorStateMachine.cpp

//================================================================
//  States   |  Description
//================================================================
//  ON       |  The motor powered on and is ready to move.
//  MOVING   |  The motor is moving
//  FAULT    |  The motor indicates a fault.
//  ALARM    |  The motor indicates an alarm state for example has reached
//           |  a limit switch.
//  OFF      |  The power on the moror drive is switched off.
//  DISABLE  |  The motor is in slave mode and disabled for normal use


namespace ExcentricMotor_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : ExcentricMotor::is_Position_allowed()
 *	Description : Execution allowed for Position attribute
 */
//--------------------------------------------------------
bool ExcentricMotor::is_Position_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for Position attribute in Write access.
	/*----- PROTECTED REGION ID(ExcentricMotor::PositionStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	ExcentricMotor::PositionStateAllowed_WRITE

	//	Not any excluded states for Position attribute in read access.
	/*----- PROTECTED REGION ID(ExcentricMotor::PositionStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	ExcentricMotor::PositionStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : ExcentricMotor::is_PresetPosition_allowed()
 *	Description : Execution allowed for PresetPosition attribute
 */
//--------------------------------------------------------
bool ExcentricMotor::is_PresetPosition_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for PresetPosition attribute in Write access.
	/*----- PROTECTED REGION ID(ExcentricMotor::PresetPositionStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	ExcentricMotor::PresetPositionStateAllowed_WRITE

	//	Not any excluded states for PresetPosition attribute in read access.
	/*----- PROTECTED REGION ID(ExcentricMotor::PresetPositionStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	ExcentricMotor::PresetPositionStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : ExcentricMotor::is_Angle_allowed()
 *	Description : Execution allowed for Angle attribute
 */
//--------------------------------------------------------
bool ExcentricMotor::is_Angle_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for Angle attribute in Write access.
	/*----- PROTECTED REGION ID(ExcentricMotor::AngleStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	ExcentricMotor::AngleStateAllowed_WRITE

	//	Not any excluded states for Angle attribute in read access.
	/*----- PROTECTED REGION ID(ExcentricMotor::AngleStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	ExcentricMotor::AngleStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : ExcentricMotor::is_SensorPosition_allowed()
 *	Description : Execution allowed for SensorPosition attribute
 */
//--------------------------------------------------------
bool ExcentricMotor::is_SensorPosition_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for SensorPosition attribute in read access.
	/*----- PROTECTED REGION ID(ExcentricMotor::SensorPositionStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	ExcentricMotor::SensorPositionStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : ExcentricMotor::is_PositionError_allowed()
 *	Description : Execution allowed for PositionError attribute
 */
//--------------------------------------------------------
bool ExcentricMotor::is_PositionError_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for PositionError attribute in read access.
	/*----- PROTECTED REGION ID(ExcentricMotor::PositionErrorStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	ExcentricMotor::PositionErrorStateAllowed_READ
	return true;
}


//=================================================
//		Commands Allowed Methods
//=================================================


/*----- PROTECTED REGION ID(ExcentricMotor::ExcentricMotorStateAllowed.AdditionalMethods) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	ExcentricMotor::ExcentricMotorStateAllowed.AdditionalMethods

}	//	End of namespace
